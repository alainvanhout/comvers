component("def-events").define(function() {

	var domEvents = ["click", 'mouseup', 'mousedown', 'mousemove'];

	function copy(origin, target) {
		target = target || {};
		if (origin) {
			Object.keys(origin).forEach(function(key) {
				target[key] = origin[key];
			});
		}
		return target;
	}

	function addDomEvent(element, eventName, fn) {
		if (element.attachEvent) {
			return element.attachEvent('on' + eventName, fn);
		} else {
			return element.addEventListener(eventName, fn, false);
		};
	}

	function EventHandler(eventName, options) {
		var _this = this;
		this.fnList = [];
		this.options = options;
		if (options && options.domEvent) {
			addDomEvent(options.domTarget, options.domEvent, function(e) {
				_this.trigger(e);
			});
		}
	}

	EventHandler.prototype = {
		trigger : function(e) {
			this.fnList.forEach(function(fn) {
				fn(e);
			});
			return this;
		},
		then : function(fn) {
			this.fnList.push(fn);
			return this;
		}
	};

	function EventsHandler(options) {
		this.eventHandlers = {};
		this.options = options;
	}

	EventsHandler.prototype.retrieveEventHandler = function(eventName, options) {
		if (!this.eventHandlers[eventName]) {
			var handler = new EventHandler(eventName, options);
			this.eventHandlers[eventName] = this[eventName] = this.options.proxy[eventName] = handler;
		}
		return this.eventHandlers[eventName];
	};

	var eventsHandlerFactory = function(options) {
		options = copy(options);
		var eventsHandler = new EventsHandler(options);
		var handlerProxy = function(eventName, handlerOptions) {
			handlerOptions = copy(options, handlerOptions);
			// only for DOM elements and when domEvent option has not been disabled
			if (handlerOptions.parent && (handlerOptions.parent instanceof HTMLElement || handlerOptions.parent == document) && handlerOptions.domEvent !== false) {
				// domEvent option is explicitly set or eventName refers to a known DOM event
				if (handlerOptions.domEvent || domEvents.indexOf(eventName) !== -1){
					// domEvent option takes priority over known DOM event
					handlerOptions.domEvent = handlerOptions.domEvent || eventName;
				}
			}
			return eventsHandler.retrieveEventHandler(eventName, handlerOptions);
		};
		options.proxy = handlerProxy;

		if (options && options.parent && options.endpoint) {
			if (options.parent[options.endpoint]) {
				console.warn("Endpoint for target already occupied");
			}
			options.parent[options.endpoint] = handlerProxy;
			options.domTarget = options.domTarget || options.parent;
		}

		return handlerProxy;
	};

	return eventsHandlerFactory;
});
