component.config.dependencies(["::comvers/comvers-amd-define.js"]);

component('dom-element-factory').dependencies({
	$ : 'jquery::https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js',
	log : 'log-util:0.0.1::general-utils/log-util.js'
}).define(function(require) {
	var $ = require('$');

	var factories = [];
	var factoryMap = {};
	var usedElementPrototype = false;

	var proxyMethods = ["add", "update", "refresh", "empty", "proxy"];
	var domEvents = ["click", "mouseup", "mousedown", "mousemove"];

	var any = function(x, fn) {
		if (Array.isArray(x)) {
			return x.map(fn);
		} else {
			return fn(x);
		}
	};

	var events = require.lenient('def-events');

	var util = {
		evaluate : function(children) {
			return any(children, function(child) {
				if ( typeof child === 'function') {
					return child();
				}
				return child;
			});
		},
		equals : function(a, b) {
			if (a === b) {
				return true;
			}
			if ( typeof a !== typeof b) {
				return false;
			}
			if (Array.isArray(a)) {
				if (a.length != b.length) {
					return false;
				}
				for (var i = 0; i < a.length; ++i) {
					if (a[i] !== b[i])
						return false;
				}
				return true;
			}
			return false;
		},
		modify : function(items, modifier) {
			if ( typeof modifier === 'function') {
				return modifier(item);
			}
			if ( typeof modifier === 'string') {
				this.assert(item, "Cannot use {0} as modifier string for null", [JSON.stringify(modifier)]);
				return item[modifier];
			}
			this.error("Mofidier is neither function nor string: {0}, item: {1}"[JSON.stringify(modifier), JSON.stringify(item)]);
		},
		add : function(input) {
			var parent = input.parent;
			var children = input.children;
			var modifier = input.modifier;
			var props = input.props;
			children = this.evaluate(children, modifier);
			if (props) {
				var keys = Object.keys(props);
				keys.forEach(function(key) {
					if (key === 'addClass') {
						parent.className += (' ' + props[key]);
					}
					if (key[0] === "_") {
						var sub = key.substring(1);
						// is there a declared event of that name?
						if (parent.when && parent.when[sub]) {
							parent.when[sub].then(props[key]);
							// is there a known dom event of that name?
						} else if (parent.when && domEvents.indexOf(sub) !== -1) {
							parent.when(sub).then(props[key]);
							// fall back on it just being an attribute
						} else {
							parent.setAttribute(key, props[key]);
						}

					} else {
						parent.setAttribute(key, props[key]);
					}
				});
			}
			if (children || modifier) {
				any(children, function(child) {
					if (child && child.tagName) {
						child = def(child);
					}
					child = util.applyStage('preAppendChild', parent, child);
					// TODO: element.asyncAdd for this? (for normal add, it messes with expectations of element.add being instantanious)
					//(window.requestAnimationFrame || window.setTimeout)(function() {
					$(parent).append(child);
					//});
				});
				if (children) {
					children.and = parent;
				}
			}
			return children;
		},
		attributeMap : function(element) {
			var map = {};
			[].slice.call(element.attributes).forEach(function(attr) {
				map[attr.name] = attr.value;
			});
			return map;
		},
		bundleArguments : function(parent, params) {
			var input = {
				parent : parent
			};
			if (params) {
				params = [].slice.call(params);
				params.forEach(function(param) {
					if (param) {
						// everything except pure objects
						if (!input.children && !( typeof param === 'object' && !param.tagName && !Array.isArray(param))) {
							input.children = param;
						} else if (!input.modifier && ( typeof param === 'string' || typeof param === 'function')) {
							input.modifier = param;
						} else if (!input.props && typeof param === 'object') {
							input.props = param;
						} else if ( typeof param !== 'undefined') {
							log.warn("failed to interpret param: " + param, typeof param);
						}
					}
				});
			}
			return input;
		},
		empty : function(parent) {
			// make sure def.raw exists
			parent.def.raw = parent.def.raw || {};
			parent.def.raw = null;
			parent.def.evaluated = null;
			$(parent).empty();
		},
		update : function(input) {
			var parent = input.parent;
			var children = input.children;
			var modifier = input.modifier;
			var props = input.props;
			raw = {
				items : children,
				modifier : modifier
			};
			// make sure def.evaluated exists
			parent.def.evaluated = parent.def.evaluated || {};
			children = this.evaluate(children);
			if (!this.equals(children, parent.def.evaluated.items)) {
				this.empty(parent);
				this.add({
					parent : parent,
					children : children
				});
				parent.def.evaluated = {
					items : children
				};
			}
			parent.def.raw = raw;
			if (raw.items && raw.items.observe){
				raw.items.observe(parent);
			}
		},
		refresh : function(parent) {
			// make sure def.raw exists
			parent.def.raw = parent.def.raw || {};
			this.update({
				parent : parent,
				children : parent.def.raw.items,
				modifier : parent.def.raw.modifier
			});
		},
		extend : function(element, factory) {
			// check if element had own element.def property (in case it gets it from Element.prototype)
			// avoid duplicate extending (which overrides/undoes proxying)
			if (Object.keys(element).indexOf('def') === -1) {
				element.def = {
					factory : {}
				};
				if (!usedElementPrototype) {
					this.basicExtend(element);
					this.extendWithChildFactories(element);
					if (element != Element.prototype) {
						// necessary because Element.prototype doesn't like it when you call element.tag
						factory = factory || factoryMap[(element.tagName || "").toLowerCase()];
						if (factory) {
							element.def.factory = factory;
							element = util.applyStage("post", element);
						}
					}
				}
				if (element) {
					element.proxy = function(target) {
						util.proxy(this, target);
					};
				}
				if (element !== Element && element !== Element.prototype && !element.when) {
					// see if def-events library is available, and if so make use of it
					events = events || require.lenient('def-events');
					if (events) {
						events({
							parent : element,
							endpoint : 'when'
						});
					}
				}
			}
		},
		basicExtend : function(element) {
			['add', 'update'].forEach(function(method) {
				element[method] = function(children, modifier, props) {
					return util[method](util.bundleArguments(this, arguments));
				};
			});
			['empty', 'refresh', 'attributeMap'].forEach(function(method) {
				element[method] = function() {
					return util[method](this);
				};
			});

			element.refresh.bound = function() {
				return function() {
					return element.refresh();
				};
			};
		},
		extendWithChildFactories : function(element) {
			factories.forEach(function(factory) {
				util.extendWithChildFactory(element, factory);
			});
		},
		extendWithChildFactory : function(element, factory) {
			element[factory.tagName] = function() {
				var children = factory.apply(null, arguments);
				this.add(children);
				return children;
			};

			element[factory.tagName + 's'] = function(children, modifier, props) {
				var _this = this;
				children = any(children, function(child) {
					var child = factory(child, modifier, props);
					_this.add(child);
					return child;
				});
				children.and = element;
				return children;
			};
			if (factory.alias) {
				element[factory.alias] = element[factory.tagName];
				element[factory.alias + 's'] = element[factory.tagName + 's'];
			}
		},
		applyStage : function(stage, parent, param) {
			if (parent.def && parent.def.factory) {
				if (parent.def.factory[stage]) {
					return parent.def.factory[stage].call(util, param);
				}
			}
			return param;
		},
		encapsulate : function(tagName) {
			return function(element) {
				if (element.tagName !== tagName) {
					return def.li(element);
				}
				return element;
			};
		},
		proxy : function(element, target) {
			proxyMethods.forEach(function(method) {
				element[method] = function() {
					target[method].apply(target, arguments);
				};
			});
		},
		createElementFactory : function(tagName) {
			var factory = function(children, modifier, props) {
				var element = document.createElement(tagName);
				def(element, factory);
				element.update(children, modifier, props);
				return element;
			};
			factory.tagName = tagName;
			factories.push(factory);
			factoryMap[tagName] = factory;
			def[tagName] = function() {
				return factory.apply(null, arguments);
			};
			def[tagName + 's'] = function(children, modifier, props) {
				return any(children, function(child) {
					return factory.apply(null, [child, modifier, props]);
				});
			};
			return factory;
		},
		retrieveElementFactory : function(tagName) {
			var factory = def[tagName];
			if (!factory) {
				factory = util.createElementFactory(tagName);
				if (usedElementPrototype) {
					util.extendWithChildFactory(Element.prototype, factory);
				}
			}
			return factory;
		},
		element : function(tagName) {
			var factory = util.retrieveElementFactory(tagName);
			return function() {
				return factory.apply(null, arguments);
			};
		},
		elements : function(tagName) {
			var factory = util.retrieveElementFactory(tagName);
			return function(children, modifier, props) {
				return any(children, function(child) {
					return factory.apply(null, [child, modifier, props]);
				});
			};
		}
	};

	var def = function(element, factory) {
		util.extend(element, factory);
		return element;
	};

	def.elementFactory = function(tagNames, options) {
		return any(tagNames, function(tagName) {
			var factory = util.retrieveElementFactory(tagName);
			if (options) {
				var keys = Object.keys(options);
				keys.forEach(function(key) {
					factory[key] = options[key];
				});
			}
			return factory;
		});
	};
	def.setAlias = function(tagName, alias) {
		log.assert(factoryMap[tagName], "Cannot set alias '{0}' for non-existent factory '{1}'", [alias, tagName]);
		factoryMap[tagName].alias = alias;
		def[alias] = def[tagName];
		def[alias + 's'] = def[tagName + 's'];
	};
	def.element = util.element;
	def.elements = util.elements;
	def.util = util;

	if (Element && Element.prototype) {
		util.extend(Element.prototype);
		usedElementPrototype = true;
	}

	return def;

});
