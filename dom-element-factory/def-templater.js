component('def-templater').version('0.0.1').dependencies({
	log : 'log-util:0.0.1::general-utils/log-util.js'
}).define(function(require) {
	var log = require('log');

	// if dom-element-factory is available, automatically make use of it
	var def = null;
	function applyDef(element) {
		def = def || require.lenient('dom-element-factory');
		if (def) {
			def(element);
		}
		return element;
	}

	// -- Template --
	function Template(input) {
		if ( typeof input === 'string') {
			this.textToDiv(input);
		}
	}


	Template.prototype = {
		textToDiv : function(sourceText) {
			log.assert(sourceText, "Template source text may not be falsy");
			this.sourceText = sourceText;
			this.templateDiv = document.createElement('div');
			this.templateDiv.innerHTML = sourceText;
		},
		getInstance : function(templateDiv) {
			log.assert(this.templateDiv, "Cannot get create instance for template without templateDiv");
			var copy = this.templateDiv.cloneNode(true);
			var instance = new TemplateInstance();
			// expose the DOM elements
			instance.nodes = copy.childNodes;
			// expose template references
			[
			].forEach.call(copy.querySelectorAll('[temp-ref]'), function(node) {
				instance.refs[node.getAttribute('temp-ref')] = applyDef(node);
			});
			return instance;
		},
	};

	// -- TemplateInstance --
	function TemplateInstance() {
		this.refs = {};
		this.nodes = null;
	}


	TemplateInstance.prototype.modify = function(input, fn) {
		var _this = this;
		// map of temp-refs -> modifier functions
		if ( typeof input === 'object' && fn == null) {
			Object.keys(input).forEach(function(key) {
				if (_this.refs[key]) {
					input[key](_this.refs[key]);
				}
			});
			return this;
		}
		// modifying the element belonging to a specific temp-ref
		if ( typeof input === 'string' && typeof fn === 'function') {
			fn(this.refs[input]);
			return this;
		}
		log.error("Cannot modify template instance based on inputs '{0}' and '{1}'", [input, fn]);
	};
	['add', 'update', 'foo'].forEach(function(methodName) {
		TemplateInstance.prototype[methodName] = function(tempRef, input) {
			var _this = this;
			_this.modify(tempRef, function() {
				log.assert(_this.refs[tempRef], "Template does not know temp-ref '{0}'", [tempRef]);
				log.assert(_this.refs[tempRef][methodName], "Method '{0}' not found on temp-ref '{1}'", [methodName, tempRef]);
				_this.refs[tempRef][methodName](input);
			});
		};
	});

	return function(sourceText) {
		return new Template(sourceText);
	};
});
