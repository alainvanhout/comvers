component("def-custom-element-factory").dependencies({
	'def' : 'dom-element-factory::dom-element-factory/dom-element-factory.js',
	'def-templater' : 'def-templater::dom-element-factory/def-templater.js'
})
.define(function(require) {
	var def = require('def');
	var templater = require('def-templater');

	var factory = {
		addElementByFunction : function(id, fn) {
			factory[id] = fn;
		},
		addElementByTemplate : function(id, templateText){
			var template = templater.textToTemplate(templateText);
			this.addElementByFunction(id, function(){
				return templater.templateToInstance(template);
			});
		}
	};
	factory.__proto__ = def;
	
	return factory;
});
