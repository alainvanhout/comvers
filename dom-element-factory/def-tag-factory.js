component("def-tag-factory").dependencies({
	dc : 'dom-change:0.0.1::dom-utils/dom-change.js'
}).define(function(require) {
	var dc = require('dc');

	var tagFactories = {};

	var tagFactory = {
		addTagType : function(tagName, properties) {
			if (properties.replacer) {
				properties.modifier = function(element){
					var replacing = properties.replacer(element);
					// transfer all attributes
					var attrs = element.attributeMap();
					Object.keys(attrs).forEach(function(key) {
						replacing.setAttribute(key, attrs[key]);
					});
					element.parentNode.replaceChild(replacing, element);
				};
			}

			tagFactories[tagName] = {
				tagName : tagName,
				modifier : properties.modifier,
				selector : properties.selector
			};
		},
		scan : function(target) {
			target = target || document.body;
			Object.keys(tagFactories).forEach(function(id) {
				var tagFactory = tagFactories[id];
				var elementList = target.querySelectorAll(tagFactory.selector);
				[].slice.call(elementList).forEach(function(element) {
					if (!(element.def && element.def.tagFactoryProcessed)) {
						tagFactory.modifier(element);
						element.def = element.def || {};
						element.def.tagFactoryProcessed = true;
					}
				});
			});
		}
	};

	// monitor dom changes
	dc(document.body, function() {
		tagFactory.scan();
	});

	// first scan when first/next animation frame is available
	window.requestAnimationFrame(function() {
		tagFactory.scan();
	});

	return tagFactory;
});
