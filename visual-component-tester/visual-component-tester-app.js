component.config.dependencies(["::comvers/comvers-messages.js", "::comvers/comvers-amd-define.js", "::comvers/comvers-diagnostics.js"]);

component("visual-component-tester-app").dependencies({
	def : "dom-element-factory::dom-element-factory/dom-element-factory.js",
	vct : 'visual-component-tester::visual-component-tester/visual-component-tester.js',
	tag : 'def-tag-factory::dom-element-factory/def-tag-factory.js',
	loremTag : 'lorem-ipsum-tag::lorem-ipsum/lorem-ipsum-tag.js',
	events : 'def-events::dom-element-factory/def-events.js',
}).define(function(require) {

	// dom maniplation
	var def = require('def');
	// use for easy <lorem-ipsum /> creation
	var loremTag = require('loremTag');
	// a visual component tester instance
	var tester = require('vct')();

	var lorem = function(wrapper) {
		var setup = {
			'min-words' : 10,
			'max-words' : 30,
		};
		if (wrapper) {
			setup['element-type'] = wrapper;
		}
		return loremTag(setup);
	};

	//inputting new lorem-ipsum paragraphs (or other element type)

	def.elementFactory(["input", "button"]);
	var body = def(document.body);

	body.button('add').when('click').then(function() {
		tester.add(lorem(elementType.value));
	});
	var elementType = body.input({
		'type' : 'text',
		value : 'p',
		size : '12'
	});

	body.add(tester);

	// initial paragraphs

	var paragraphs = Array(2).join(".").split('.').map(function() {
		// add lorem-ipsum element -> paragraph with lorem ipsum content
		return lorem('p');
	});
	tester.add(paragraphs);

}).run();
