component.config.dependencies(["::comvers/comvers-amd-define.js"]);

component("visual-component-tester").dependencies({
	css : "::visual-component-tester/visual-component-tester.css",
	template : "::visual-component-tester/visual-component-tester.html>text",
	def : "dom-element-factory::dom-element-factory/dom-element-factory.js",
	templater : "def-templater:0.0.1::dom-element-factory/def-templater.js",
	$ : 'jquery::https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.jsjquery/jquery.js',
	events : 'def-events::dom-element-factory/def-events.js'
}).define(function(require) {

	var def = require("def");
	var $ = require("$");
	var templater = require("templater");
	var templateText = require("template");

	return function() {

		var instance = templater(templateText).getInstance();

		var innerDiv = instance.refs.inner;

		// resizing

		var moving = false;
		var startX, startY, startWidth, startHeight;

		var setSize = function(width, height) {
			$(innerDiv).width(width);
			$(innerDiv).height(height);
		};

		instance.refs.mover.when('mousedown').then(function(e) {
			moving = true;
			startX = e.pageX;
			startY = e.pageY;
			startWidth = $(innerDiv).width();
			startHeight = $(innerDiv).height();
		});
		def(document).when('mouseup').then(function(e) {
			moving = false;
		});

		def(document).when('mousemove').then(function(e) {
			if (moving) {
				setSize(startWidth - startX + e.pageX, startHeight - startY + e.pageY);
			}
		});

		// buttons

		[].slice.call(instance.refs.buttons.children).forEach(function(button) {
			def(button);
			var attrs = button.attributeMap();
			button.when('click').then(function() {
				setSize(attrs['tester-width'], attrs['tester-height']);
			});
		});

		// compent API

		var container = instance.refs.container;
		container.proxy(innerDiv);
		container.setSize = setSize;

		return container;
	};
});
