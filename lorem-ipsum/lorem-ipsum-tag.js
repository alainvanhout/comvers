component('lorem-ipsum-tag').dependencies({
	def : "dom-element-factory::dom-element-factory/dom-element-factory.js",
	lorem : 'lorem-ipsum::lorem-ipsum/lorem-ipsum.js',
	tag : 'def-tag-factory::dom-element-factory/def-tag-factory.js'
}).define(function(require) {
	var lorem = require('lorem');
	var tag = require('tag');
	var def = require('def');

	var examine = function(element) {
		var attrs = element.attributeMap();
		return {
			minWords : parseInt(attrs['min-words'] || attrs['max-words'] || attrs.words || 5),
			maxWords : parseInt(attrs['max-words'] || attrs['min-words'] || attrs.words || 50),
			// lorem ipsum text will always be encapsulated in an element, either a specified one of a span as fallback
			elementType : attrs['element-type'] || 'span'
		};
	};

	tag.addTagType('lorem-ipsum replace', {
		selector : 'lorem-ipsum',
		replacer : function(element) {
			var setup = examine(element);
			return def.element(setup.elementType)(lorem.words(setup.minWords, setup.maxWords));
		}
	});
	tag.addTagType('lorem-ipsum fill', {
		selector : '[lorem-ipsum]',
		modifier : function(element) {
			var setup = examine(element);
			def(element).update(lorem.words(setup.minWords, setup.maxWords));
		}
	});

	return function() {
		return def.element('lorem-ipsum').apply(null, arguments);
	};
}).run();

