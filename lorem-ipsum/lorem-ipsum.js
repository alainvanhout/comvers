component('lorem-ipsum').dependencies({
	text : '::lorem-ipsum/lorem-ipsum.txt>text'
}).define(function(require) {
	var text = require('text');
	var split = null;

	var randomSize = function(min, max) {
		max = max || min;
		return count = min + Math.round((max - min) * Math.random());
	};
	
	var properCaps = function(s){
		return s.charAt(0).toUpperCase() + s.slice(1);
	};

	var lorem = function(minLength, maxLength) {
		var length = randomSize(minLength, maxLength);
		if (length) {
			return text.substring(0, length);
		}
		return text;
	};
	lorem.words = function(minCount, maxCount) {
		split = split || text.split(' ');
		if (this.shuffle){
			split = split.sort(function(){
				return Math.random();
			});
		}
		var count = randomSize(minCount, maxCount);
		var subText = split.slice(0, count).join(' ');
		// remove trailing comma, if any
		return properCaps(subText.replace(/(,$)/g, ""));
	};

	return lorem;
});