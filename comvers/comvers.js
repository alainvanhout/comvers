component = function() {
	// the runner queue: a list of the currently to be executed component runs
	var q = [];
	// repository of all component version sets
	var repo = {};
	// loading/loaded sources (to avoid duplicate loading whenever possible)
	var srcs = [];
	// string constant, since it's used in several places
	var df = 'default';
	// messages: can be added to to make the warning or error messages understandable
	var ms = {};

	// utility methods
	var u = {
		// checks whether test fails, and calls error with message and parameters if it does
		assert : function(t, m, ps) {
			t || this.error(m, ps);
		},
		// throws error with message with parameters
		error : function(m, ps) {
			throw Error(this.format(m, ps));
		},
		// checks whether test fails, and logs message with parameters if it does
		warn : function(t, m, ps) {
			t || console.log(this.format(m, ps));
		},
		// inserts parameters in message and uses understandable message if one is defined for the message code
		format : function(m, ps) {
			// check if a message is defined for the message code
			if (ms[m]) {
				// return the message code + the message with inserted arguments
				return m + " > " + ms[m].replace(/{(\d+)}/g, function(p, n) {
					return ps[n] ? ps[n] : p;
				});
			}
			// default the message code itself and a list of the parameters
			return m + " > " + ps.join(", ");
		},
		// for the provided version set, this return the version itself, or first available (if any), or the default version
		resolveVersion : function(vs, v) {
			// if a version is declared, use that one, otherwise set function for finding any non-default version
			v = v || function(k) {
				return k !== df;
			};
			// if the version is not a version function use that
			// otherwise, just the function as filter
			// and use 'default' as a fallback
			return ( typeof v !== 'function' ? v : Object.keys(vs.versions).filter(function(k) {
				return vs.versions[k].v ? v(vs.versions[k].v): false;
			})[0]) || df;
		},
		// retrieves the component version set, and makes it if needed and specified
		getVersionSet : function(id, mk) {
			mk && (repo[id] = repo[id] || {
				id : id,
				versions : {}
			});
			this.assert(repo[id], 'vsnf', [id]);
			return repo[id];
		},
		// retrieves a component of a specific version, and makes it if needed and specified
		// takes a version set object and a version string as input
		getVersion : function(vs, v, mk) {
			v = this.resolveVersion(vs, v);
			mk && (vs.versions[v] = vs.versions[v] || new Component(vs.id, v));
			// at this point, the component version should exist
			this.assert(vs.versions[v], 'vnf', [vs.id, v]);
			return vs.versions[v];
		},
		// short-cut retrieval of component based on id string and version string
		// also makes the component it if needed and specified
		getComponent : function(id, v, mk) {
			// no point is asserting anything here, since getVersion and getVersionSet already do that
			return this.getVersion(this.getVersionSet(id, mk), v, mk);
		},
		// loads the dependencies of a specific component
		// takes a component object as input
		loadDependencies : function(c) {
			// the given component should not be null
			this.assert(c, 'ldn');
			// check if *all* dependencies are loaded
			// this also initiates loading of each dependency if needed (i.e. not loaded yet) and possible (i.e. src provided)
			c.dependenciesLoaded = Object.keys(c.dependencySet).reduce(function(loaded, k) {
				// specific dependency
				var d = c.dependencySet[k];
				// component belonging to the dependency (create if needed)
				var dc = u.getComponent(d.id, d.version, true);
				// make sure component knows its source and type (if any)
				dc.src = d.src || dc.src;
				dc.type = dc.type || d.type;
				// reduce loaded value
				return loaded && u.loadComponent(dc);
			}, true);
			// ensure life cycle event log exists if dependencies are indeed loaded by now
			if (c.dependenciesLoaded) {
				c.did("dependenciesLoaded");
				// remove loading timeout
				clearTimeout(c.timeout);
				return true;
			}
			// all dependencies of component should be load within 5 seconds (or overridden value)
			// if not, throw error
			// save timeout id to ensure that only one timeout is created per component
			c.timeoutid = c.timeoutid || setTimeout(function() {
				c.did("loadingTimeout");
				u.assert(c.dependenciesLoaded, 'dlt', [c.id, c.v]);
			}, c.timeout || conf.timeout || 5000);
			// implicitely return false/null
		},
		// loads a component's dependencies and, if those are done, its source file (if available)
		// the loading of the source may add (further) dependencies to the component
		loadComponent : function(c) {
			// check if dependency and resource are loaded (if component was declared with source, not (yet) a definition)
			return this.loadDependencies(c) && this.loadResource(c);
		},
		// loads the source file if available and needed
		loadResource : function(c) {
			// check whether loading of resource still needs to be initiated
			if (u.needsLoading(c)) {
				u.loadSource(c);
				c.loadingResource = true;
			}
			// a component is deemed loaded when the source file is loaded or the component has a definition
			return c.definition || c.loadedResource;
		},
		// check if source string is available and component not already loaded (or loading)
		// check list of loading/loaded sources to avoid duplicate loading whenever possible
		needsLoading : function(c) {
			return c.src && !c.loadingResource && !c.loadedResource && srcs.indexOf(c.src) === -1;
		},
		// the actual loading of the source file of a component
		loadSource : function(c) {
			srcs.push(c.src);
			var e;
			// add scrip tag for js source file
			if (c.type === 'js') {
				e = document.createElement('script');
				e.src = c.src;
				e.async = "asynch";
				e.type = "application/javascript";
			}// add link tag for css source file
			else if (c.type === 'css') {
				e = document.createElement('link');
				e.href = c.src;
				e.type = "text/css";
				e.rel = "stylesheet";
			}// use xhr for text retrieval of e.g. a template
			else if (c.type === 'text') {
				var xhr = new XMLHttpRequest();
				xhr.open('GET', c.src);
				xhr.onreadystatechange = function() {
					if (xhr.readyState === 4) {
						c.define(function() {
							return xhr.responseText;
						});
						u.afterLoad(c);
					}
				};
				xhr.send();
				return;
			}// no other dependency types supported
			else {
				this.error('rtnf', [c.type, c.id, c.v]);
			}
			// add element reference to component (for debugging or advanced usage)
			c.e = e;
			// once the source file is loaded recheck queue
			e.addEventListener("load", function() {
				u.afterLoad(c);
			});
			document.head.appendChild(e);
		},
		// what should be done after the loading of a component's source file
		// seperated out to allow easy override
		afterLoad : function(c) {
			c.loadedResource = true;
			u.checkQueue();
		},
		// runs any scheduled component-runs present in the queue
		checkQueue : function() {
			// force run-check to be synchronous/consecutive
			setTimeout(function() {
				// main config component should have all its dependencies loaded and have an instantiation (i.e. have a run)
				// though of course only if it has an actual definition
				// if that's in order, do runs that are possible and retain those that are not yet possible
				u.loadDependencies(conf) && (!conf.definition || u.getInstance(conf)) && ( q = q.filter(function(i) {
					// use q.filter to retain the components that have not yet fully loaded
					if (!u.loadComponent(i.comp)) {
						return true;
					}
					// run all the other components, with the provided input (if any)
					u.getInstance(i.comp, i.input);

					// the component's dependencies may have changed during the run -> check if it still checks out as loaded.
					// if not, nullify instantiation and rerun at a later time
					if (!u.loadComponent(i.comp)) {
						u.definition = null;
						return true;
					}
				}));
			});
		},
		// add a component run, with input if specified
		run : function(c, i) {
			// force run addition to be synchronous/consecutive
			setTimeout(function() {
				q.push({
					comp : c,
					input : i
				});
				// once it's added, again try to run queued component-runs
				u.checkQueue();
			});
		},
		// create an instance of a component, with input if specified
		createInstance : function(c, i) {
			// the component definition should exist at this point
			this.assert(c.definition, 'dfnf', [c.id, c.v]);
			// run the component's definition with as parameters: a require function and the specified input
			// the require function looks up the provided alias in the component's dependency declaration
			// if the alias is not found or the dependency isn't loaded as expected, an exception is thrown
			var r = function(a, i) {
				return u.require(c, a, i);
			};
			// also allow lenient global require, i.e. returns null if non-existent in global component repository
			// the component reference 's' uses the same syntax as single-line dependency declaration (see u.parseReference)
			r.lenient = function(s, i) {
				var d = u.parseReference(null, s);
				var vs = repo[d.id];
				var v = vs ? u.resolveVersion(vs, d.version) : null;
				var dc = vs && vs.versions[v] ? vs.versions[v] : null;
				return dc && dc.definition && dc.dependenciesLoaded ? u.getInstance(dc, i) : null;
			};
			return c.definition(r, i);
		},
		// retrieves the component's instantiation if already available and makes an instatiation if needed
		// this instatiation is an instance based on null as specified input
		getInstance : function(c, i) {
			c.did("firstInstanceFetch");
			// if an input is specified, always create new instance -> use require("foo", true) to circumvent the default singleton behaviour
			// otherwise use instantiation and create it if it isn't yet available
			return i ? this.createInstance(c, i) : (c.instantiation || (c.instantiation = this.createInstance(c, i) || true));
		},
		// returns component instances based on
		require : function(c, a, i) {
			// retrieve the dependency based on the provided alias
			var d = c.dependencySet[a];
			// this dependency should be null
			u.assert(d, 'dnf', [a, c.id, c.v]);
			// cache the component for debugging (but always re-acquire it to allow advanced usage where dependencies may change)
			d.component = u.getComponent(d.id, d.version);
			// keep a list of which components actually ask for (i.e require()) an instance of the component that reflects this dependency
			d.component.usedBy = d.component.usedBy || [];
			d.component.usedBy.push(c.id + ":" + c.v);
			// at this point a component (i.e. specific version) should have been found
			u.assert(d.component, 'dvnf', [d.id, d.version, c.id, c.v]);
			// retrieve instantation (if available) or new instance of the component (if no instantiation is yet available of if input is provided)
			return u.getInstance(d.component, i);
		},
		parseReference : function(a, setup) {
			//// various formats:
			// id:version::src
			// id:version <- no source available
			// id::src <- use any version that is available
			// :version::source <- src is used as id (with alias as fallback) 
			// ::src <- src is used as id (with alias as fallback) + use any version that is available (more likely used to just load a specific file)
			var r = setup.split("::");
			var r1 = r[0].split(":");
			var r2 = r.length > 1 ? r[1].split(">") : [];
			var src = r2[0];
			return {
				// id is the first part before ':', with src as fallback, and alias as a final fallback
				id : r1[0] || src || a,
				// version is the second part after ':' with null as fallback
				// null is interpreted as first/any available version, or 'default' as fallback
				version : r1.length > 1 ? r1[1] : null,
				// source is second part, after '::'
				src : src,
				// type is optionally specified
				type : r2.length > 1 ? r2[1] : null
			};
		}
	};
	// create a component dependency based on a alias
	var ComponentDependency = function(a, setup) {
		this.alias = a;
		// string representation is of the form id:version::source
		// both version and source are in theory optional (i.e. 'id::source' or 'id:version')
		// id is also optional, in which case the alias is used (i.e. 'alias' : ':version::source')
		// if only source is provided alias is taken as id and null as version ('alias' : 'source')
		typeof setup === "string" && ( setup = u.parseReference(a, setup));
		// alias is used as a fallback for id
		this.id = setup.id || a;
		this.version = setup.version;
		this.src = setup.src;
		// if source is specified then type is as specified, or guessed based on file extension
		this.src && (this.type = setup.type || this.src.substring(this.src.lastIndexOf(".") + 1) || "js");
	};

	var Component = function(id, v) {
		// the identifier/name
		this.id = id;
		// the version
		this.v = v;
		// the set of dependencies
		this.dependencySet = {};
		// the life cycle event of the object (when it was defined, loaded, first called, etc)
		this.lifeCycle = {};
		// callback hooks for reactions to lifeCycle events (e.g. c.react.foo=function(c){...} )
		this.react = {};
		// set first life cycle event log (this lifeCycle event can inherently not be given a component-specific react hook)
		this.did("created");
	};
	Component.prototype = {
		// add a definition to the component
		define : function(def) {
			// soft warning that the component has received a definition more than once
			u.warn(!this.definition, 'cad', [this.id, this.v]);
			this.definition = def;
			// set first life cycle event log
			this.did("defined");
			u.afterLoad(this);
			return this;
		},
		// set datestamp for specified life cycle event, if it hasn't yet been set
		// reflects miliseconds after comvers was loaded (see bottom of file)
		did : function(stage) {
			// see if lifeCycle event hook exist on this component itself or on the global component object
			var r = this.react[stage] || mng.react[stage];
			// execute the lifeCycle event hook callback if there is one
			r && r(this);
			// now register the lifeCycle event (note: only the first one is actually logged)
			this.lifeCycle[stage] |= (new Date()).getTime() - mng.started.getTime();
		},
		// adding dependencies to the component
		dependencies : function(ds) {
			var _this = this;
			// if the dependencies are supplied as an anonymous function, then first flatten it
			// this allows for easy encapsulated access to the component itself via 'this' 
			typeof ds === 'function' && (ds = ds.call(this));
			// take an array of strings and add each to the dependency set as both alias and description
			if (Array.isArray(ds)) {
				ds.forEach(function(d) {
					var d = new ComponentDependency(null, d);
					// id serves as alias
					d.alias = d.id;
					_this.dependencySet[d.id] = d;
				});
			} else {
				// straghtforward adding of dependencies to set
				Object.keys(ds || {}).forEach(function(a) {
					_this.dependencySet[a] = new ComponentDependency(a, ds[a]);
				});
			}
			this.dependenciesLoaded = false;
			return this;
		},
		// returns the component's sybling with the given version, and created it if needed
		version : function(v) {
			return u.getVersion(repo[this.id], v, true);
		},
		// setting the source file of the component
		// this is an alternative to having it provided via other component's dependeny descriptions
		source : function(src) {
			this.src = src;
			// guessing type based on file extension
			this.type = src.substring(src.lastIndexOf(".") + 1) || "js";
			return this;
		},
		// adding a component run to the queue
		run : function(i) {
			return u.run(this, i);
		},
		// immediately executed the definition to return an instance
		// note that this does not first load the component's source or it's dependencies
		// so use at your own perril (e.g. for dependency-less resources that need to skip the queue)
		instance : function(i){
			return u.getInstance(this, i);
		}
	};
	// the to-be-exposed function that retrieves the default version of a specified component
	// and creates that default version if needed
	var mng = function(id) {
		return u.getComponent(id, df, true);
	};
	// exposing the internals
	mng.repository = repo;
	mng.queue = q;
	mng.sources = srcs;
	mng.util = u;
	mng.messages = ms;
	mng.loadedSources = srcs;
	// lifeCycle react hook across all components (can be overridden by c.react.foo = function(c){...})
	mng.react = {};
	// take this as starting moment for component life cycle event logging
	mng.started = new Date();
	// main config component for global dependency/resource loading and performing setup code
	var conf = mng.config = mng('comvers-config');
	return mng;
}();

