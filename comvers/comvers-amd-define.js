(function() {
	component.messages["crnf"] = "Require unsuccessful: {0} not found";
	component.messages["ard"] = "Avoided re-defining via exports for '{0}:{1}'";

	// access to comvers internals
	var u = component.util;
	var srcs = component.loadedSources;
	var repo = component.repository;

	// amd-comptability extension
	window.define = function(id, dependencies, definition) {
		var setup = {
			id : id,
			dependencies : dependencies,
			definition : definition
		};
		// allow specific handlers to pre-process the input
		define.handlers.forEach(function(handler) {
			handler(setup);
		});
		// todo: encapsulate definition to translate between amd and comvers
		component(setup.id).version(setup.version).dependencies(setup.dependencies).define(setup.definition);
	};
	// allows jquery to know that this is / acts like an amd module loader
	define.amd = true;
	define.handlers = [];
	
	define.handlers.push(function(setup) {
		if (setup.definition && typeof setup.definition === 'function' && setup.definition().fn) {
			setup.version = setup.definition().fn.jquery;
		}
	});
	
	// related a given source to a specific component id and version
	var sourceMap = {};
})();
