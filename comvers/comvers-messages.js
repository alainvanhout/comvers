(function(){
	var m = component.messages;
	
	m["cad"] = "Component {0}:{1} was already defined";
	m["vsnf"] = "Version set not found for {0}";
	m["vnf"] = "Version not found for {0}:{1}";
	m["ldn"] = "Cannot load dependencies of null";
	m["dlt"] = "Dependency loading timeout: {0}:{1}";
	m["rtnf"] = "Resource type {0} not found for {1}:{2}";
	m["dfnf"] = "Definition not found for {0}:{1}";
	m["dnf"] = "Dependency {0} not found for {1}:{2}";
	m["dvnf"] = "Dependency {0}:{1} not found for {2}{3}";
	m["rcf"] = "Resource checking failed for {0}";

}());

