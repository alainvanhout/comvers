component('comvers-diagnostics').define(function(require, input) {
	var facets = ["main", "fullyLoaded", "notFullyLoaded", "shallowParents", "deepParents", "shallowChildren", "deepChildren", "idle", "timeline"];
	input = input || facets;

	function show(facet) {
		return input.indexOf(facet) > -1;
	}

	if (show("options") || input === "options") {
		console.log(facets);
	}

	var repo = component.repository;

	function lvl(level) {
		return Array(level * 2 + 1).join(" ");
	}

	// flat list of all components
	var components = [];

	// fully loaded components
	var loadedComponents = [];
	// components that have not fully loaded (yet)
	var unloadedComponents = [];
	// set: each item contains a list of the components that have the component (i.e. set key)
	// as a direct dependency
	var shallowParents = {};
	// set: each item contains a list of the components that have the component (i.e. set key)
	// as a direct or indirect dependency
	var deepParents = {};
	// set: each item contains a list of the components that are
	// a direct dependency of the component (i.e. set key)
	var shallowChildren = {};
	// set: each item contains a list of the components that are
	// a direct or indirect dependency of the component (i.e. set key)
	var deepChildren = {};
	// full timeline based on lifeCycles
	var timeline = [];

	if (show("main")) {
		console.log("Loaded status");
	}
	// loop version sets
	Object.keys(repo).forEach(function(id) {
		var versionSet = repo[id];
		if (show("main")) {
			console.log(lvl(1), id);
		}
		// loop versions
		Object.keys(versionSet.versions).forEach(function(version) {
			var comp = versionSet.versions[version];
			var compId = comp.id + ":" + (comp.v || 'default');

			components.push(comp);

			var ar = ["defined", "firstInstanceFetch"];
			// timeline
			Object.keys(comp.lifeCycle).forEach(function(event) {
				if (ar.indexOf(event) > -1) {
					timeline.push({
						compId : compId,
						event : event,
						time : comp.lifeCycle[event]
					});
				}
			});

			// make sure all components are represented in the lists
			shallowParents[compId] = shallowParents[compId] || [];
			deepParents[compId] = deepParents[compId] || [];
			shallowChildren[compId] = shallowChildren[compId] || [];
			deepChildren[compId] = deepChildren[compId] || [];

			// make an array of the applicable states
			var state = [];
			comp.loadingResource && state.push("loading resource");
			comp.loadedResource && state.push("loaded resource");
			comp.definition && state.push("has definition");
			comp.dependenciesLoaded && state.push("dependencies loaded");

			if (show("main")) {
				// overall summary of the components
				console.log(lvl(2), "-", comp.v, state);
				console.log(lvl(3), [].slice.call(Object.keys(comp.lifeCycle)).map(function(key) {
					return key + ": " + comp.lifeCycle[key];
				}));
			}

			// deemed loaded if it has a definition or if its source file has loaded
			if (comp.loadedResource || comp.definition) {
				loadedComponents.push(comp);
			} else {
				unloadedComponents.push(comp);
			}

			// loop dependencies (non-exhaustive for now)
			// include both deep and shallow lists in this sweep
			Object.keys(comp.dependencySet).forEach(function(alias) {
				var dep = comp.dependencySet[alias];
				var depId = dep.id + ":" + (dep.version || 'default');
				if (dep.component){
					depId = depId || dep.component.id + ":" + (dep.component.version);
				}
				shallowParents[depId] = shallowParents[depId] || [];
				shallowParents[depId].push(compId);

				deepParents[depId] = deepParents[depId] || [];
				deepParents[depId].push(compId);

				shallowChildren[compId] = shallowChildren[compId] || [];
				shallowChildren[compId].push(depId);

				deepChildren[compId] = deepChildren[compId] || [];
				deepChildren[compId].push(depId);
			});
		});
	});
	if (show("main")) {
		console.log("");
	}

	// list of fully loaded components
	if (show("fullyLoaded")) {
		console.log("Fully loaded");
		loadedComponents.forEach(function(comp) {
			var compId = comp.id + ":" + (comp.v || 'default');
			console.log(lvl(2), compId);
		});

		console.log("");
	}

	// list of not fully loaded components
	if (show("notFullyLoaded")) {
		console.log("Not fully loaded");
		unloadedComponents.forEach(function(comp) {
			var state = [];

			// add some state description

			(comp.loadingResource && !comp.loadedResource) && state.push("failed loading resource");
			!comp.dependenciesLoaded && state.push("dependencies not loaded");
			if (comp.type === 'js' || !comp.type) {
				(comp.dependenciesLoaded && !comp.definition) && state.push("has no definition");
				(!comp.src && !comp.definition) && state.push("no source available");
			}

			var compId = comp.id + ":" + (comp.v || 'default');
			if (comp.v === 'default' && comp.definition == null && (comp.type === 'js' || comp.type == null)) {
				console.log(lvl(2), compId, state);
			} else {
				console.warn(lvl(2), compId, state, "<-- possible issue");
			}
		});

		console.log("");
	}

	// shallow parents
	if (show("shallowParents")) {
		console.log("Shallow parent relations:");
		Object.keys(shallowParents).forEach(function(dep) {
			console.log(lvl(1), dep, "used by", shallowParents[dep]);
		});
	}
	console.log("");

	// deep parents
	if (show("deepParents")) {
		var changed = true;
		while (changed) {
			changed = false;
			Object.keys(deepParents).forEach(function(key) {
				var parents = deepParents[key];
				// loop over the dependency's parents
				parents.forEach(function(parent) {
					var grandParents = deepParents[parent] || [];
					grandParents.forEach(function(grandParent) {
						if (parents.indexOf(grandParent) === -1) {
							parents.push(grandParent);
							changed = true;
						}
					});
				});
			});
		}
		console.log("Deep parent relations:");
		Object.keys(deepParents).forEach(function(dep) {
			console.log(lvl(1), dep, "used by", deepParents[dep]);
		});

		console.log("");
	}

	// shallow children
	if (show("shallowChildren")) {
		console.log("Shallow child relations:");
		Object.keys(shallowChildren).forEach(function(dep) {
			console.log(lvl(1), dep, "uses", shallowChildren[dep]);
		});

		console.log("");
	}

	// deep children
	if (show("deepChildren")) {
		changed = true;
		while (changed) {
			changed = false;
			Object.keys(deepChildren).forEach(function(key) {
				var children = deepChildren[key];
				// loop over the dependency's children
				children.forEach(function(child) {
					var grandChildren = deepChildren[child] || [];
					grandChildren.forEach(function(grandChild) {
						if (children.indexOf(grandChild) === -1) {
							children.push(grandChild);
							changed = true;
						}
					});
				});
			});
		}
		console.log("Deep child relations:");
		Object.keys(deepChildren).forEach(function(dep) {
			console.log(lvl(1), dep, "uses", deepChildren[dep]);
		});

		console.log("");
	}

	// idle components
	if (show("idle")) {
		console.log("Idle components (no parents, children or instantiation):");
		components.forEach(function(comp) {
			var compId = comp.id + ":" + (comp.v || 'default');
			if (shallowChildren[compId].length == 0 && shallowParents[compId].length == 0 && comp.lifeCycle.firstInstanceFetch == null) {
				if (comp.v === 'default' && comp.definition == null) {
					console.log(lvl(1), compId);
				} else {
					console.warn(lvl(1), compId, "<-- possible issue");
				}
			}
		});

		console.log("");
	}

	if (show("timeline")) {
		timeline.sort(function(a, b) {
			return a.time - b.time;
		});
		timeline.forEach(function(i) {
			console.log(i.time + " : " + i.compId + " > " + i.event);
		});
	}
});
