component('observable').version('0.0.1').define(function() {
	return function (obj, container){
		// exposed as object property
		obj.observers = [];
		obj.observe = function(observer){
			// avoid duplicate entries
			if (this.observers.indexOf(observer) === -1){
				this.observers.push(observer);
			}
		};
		obj.refresh = function(){
			// alert all observers that they should refresh
			this.observers.forEach(function(observer){
				observer.refresh();
			});
		};
		// add to container, if a container was provided
		if (container && container.indexOf(obj) == -1){
			container.push(obj);	
		}
		return obj;
	};
});
