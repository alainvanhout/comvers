component('translator').version('0.0.1').version('0.0.1').dependencies({
	log : 'log-util:0.0.1::general-utils/log-util.js',
	observable : 'observable:0.0.1::general-utils/observable.js'
}).define(function(require) {
	var log = require('log');
	var observable = require('observable');
	
	function forKeys(obj, fn){
		Object.keys(obj).forEach(function(key){
			fn(key, obj[key]);
		});
	}
	

	function Language(name, parent) {
		this.name = name;
		this.parent = parent;
		this.terms = {};
	};
	Language.prototype = {
		set : function(key, translation) {
			log.assert(key !== null, "Translation key may not be null");
			this.terms[key] = translation;
			this.parent.refresh(key);
		},
		setAll : function(items) {
			log.assert(typeof items === 'object', "Given translation items must be of type object: {0}", [items]);
			forKeys(items, this.set.bind(this));
		},
		get : function(key){
			log.assert(this.terms[key], "Translation not known for item '{0}' and language '{1}'", [key, this.name]);
			return this.terms[key];
		}
	};
	
	var t = function(key) {
		return !t.wrap ? t.get(key) : t.observables[key] = t.observables[key] || observable(function() {
			return t.get(key);
		});
	};
	t.currentLanguage = null;
	t.wrap = false;
	t.languages = {};
	t.observed = [];
	t.observables = {};
	t.language = function(language) {
		return this.languages[language] = this.languages[language] || new Language(language, this);
	};
	t.setLanguage = function(name) {
		log.assert(t.languages[name], "Language '{0}' not among declared languages: {1}", [name, Object.keys(t.languages)]);
		this.currentLanguage = name;
		forKeys(this.observables, function(key, observable){
			observable.refresh();
		});
	};
	t.setAll = function(items) {
		forKeys(items, function(key, item) {
			t.set(key, item);
		});
	};
	t.set = function(key, item) {
		forKeys(item, function(language, value) {
			t.languages[language].set(key, value);
		});
	};
	t.refresh = function(key){
		if (this.observables[key]){
			this.observables[key].refresh();
		}
	};
	
	t.get = function(key) {
		log.assert(t.currentLanguage, "Language is not set");
		return t.language(t.currentLanguage).get(key);
	};
	
	return t;
});
