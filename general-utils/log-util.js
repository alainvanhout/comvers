component('log-util').version('0.0.1').define(function() {
	return {
		// checks whether test fails, and calls error with message and parameters if it does
		assert : function(t, m, ps) {
			t || this.error(m, ps);
		},
		// throws error with message and parameters
		error : function(m, ps) {
			throw Error(this.format(m, ps));
		},
		// checks whether test fails, and logs message with parameters if it does
		warn : function(t, m, ps) {
			t || console.log(this.format(m, ps));
		},
		// inserts parameters in message
		format : function(m, ps) {
			return m.replace(/{(\d+)}/g, function(p, n) {
				return ps[n] ? ps[n] : p;
			});
		}
	};
});
